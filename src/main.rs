use clap::{App, Arg};
mod lib; 

fn main() {
    let matches = App::new("My CLI Tool")
        .version("1.0")
        .author("Your Name")
        .about("Does awesome things")
        .arg(Arg::with_name("INPUT")
             .help("Sets the input number to process")
             .required(true)
             .index(1))
        .get_matches();

    let input = matches.value_of("INPUT").unwrap().parse::<i32>().expect("Input must be a number");
    let processed = lib::process_data(input);
    println!("Processed number: {}", processed);
}



