#!/bin/bash

# Set OpenSSL environment variables
export OPENSSL_INCLUDE_DIR=$(brew --prefix openssl)/include
export OPENSSL_LIB_DIR=$(brew --prefix openssl)/lib

echo "OPENSSL_INCLUDE_DIR set to $OPENSSL_INCLUDE_DIR"
echo "OPENSSL_LIB_DIR set to $OPENSSL_LIB_DIR"

