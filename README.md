## Prerequisites

Before starting, ensure you have the following installed on your machine:
- Rust and Cargo (Follow the [official installation guide](https://www.rust-lang.org/tools/install))
- Docker (Refer to the [Docker installation guide](https://docs.docker.com/get-docker/))

## Building the Project

1. **Create the Project** (if starting from scratch):
   ```bash
   cargo new project8 --bin
   ```
   This command creates a new binary project named `project8`.

2. **Build the Project**:
   Navigate to the project directory:
   ```bash
   cd project8
   ```
   Then compile the project with:
   ```bash
   cargo build
   ```

## Running the Project

Start the project using Cargo, Running CLI tool
```bash
cargo run -- n
```
This command compiles (if necessary) and runs the `project8` application.

## Testing the Project

Run the Unit Test

```bash
cargo test
```


## Dockerizing the Application

To containerize `project8`, follow these steps:

### 1. **Create a Dockerfile**

Create a `Dockerfile` in the root directory of your project with the following content:
```Dockerfile
# Build stage
FROM rust:1.67 as build
WORKDIR /usr/src/project8
COPY . .
RUN cargo build --release

# Runtime stage
FROM debian:buster-slim
COPY --from=build /usr/src/project8/target/release/project8 /usr/local/bin/project8
EXPOSE 8080
CMD ["project8"]
```

### 2. **Build the Docker Image**

From the root directory of your project, run:
```bash
docker build -t project8 .
```
This command builds a Docker image named `project8` from your Dockerfile.

### 3. **Run a Container**

Launch a container from your image:
```bash
docker run -d -p 8080:8080 --name project8_container project8
```
This runs your application in a container named `project8_container`, mapping port 8080 of the container to port 8080 on the host.

### 4. **Viewing Logs**

To check the logs of the running container, use:
```bash
docker logs project8_container
```

### 5. **Stopping and Removing the Container**

Stop the container with:
```bash
docker stop project8_container
```
And remove it with:
```bash
docker rm project8_container
```

## Screen shots:

-  Build the project
    - ![Alt text](./images/6.jpg "Optional title1")

-  Running CLI tool
    - ![Alt text](./images/7.jpg "Optional title1")

-  Test the function
    - ![Alt text](./images/8.jpg "Optional title0")

